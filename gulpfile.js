var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var jshint = require('gulp-jshint'); // Notify on JS errors
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var gutil = require('gulp-util'); // used to check the param passed from command line
var gulpif = require('gulp-if'); // Conditionally run a task
var stripDebug = require('gulp-strip-debug'); // Strip out console.log messages etc
var concat = require('gulp-concat'); // combines the JS int one file
var filter = require('gulp-filter'); // ensures that only *.css files ever reach .reload - this way we'll still get CSS injecting.
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var sizereport = require('gulp-sizereport');
var rename = require('gulp-rename');
var stripCssComments = require('gulp-strip-css-comments');
var path = require('path');
var babel = require('gulp-babel'); // ES6
var inject = require('gulp-inject');

// Set some defaults
var isDev = true;
var isProd = false;

// If "prod" is passed from the command line then update the defaults
if (gutil.env._[0] === 'prod') {
  isDev = false;
  isProd = true;
}

location = './source/';
destination = location;
jsFileName = 'script.min.js';

var plumberErrorHandler = {
  errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

// Gulp Sass Task
gulp.task('sass', function() {
  return gulp
    .src(location + '/sass/*.{scss,sass}')
    .pipe(plumber(plumberErrorHandler))
    .pipe(inject(gulp.src(location + '/_patterns/**/sass/*.scss', { read: false }), {
      starttag: '/* inject:imports */',
      endtag: '/* endinject */',
      relative: true,
      transform: function (filepath) {
        return '@import "' + filepath + '";';
      }
    }))
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: [
          'node_modules/breakpoint-sass/stylesheets',
          location + '/sass/1-base'// this expose mixins and variables
        ]
      })
    )
    .pipe(
      autoprefixer({
        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
      })
    )
    .pipe(gulpif(isProd, cleanCSS()))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(destination + '/css'))
    .pipe(filter('**/*.css')) // Filtering stream to only css files
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('sizereport', function() {
  return gulp.src(destination + '/**/*.css').pipe(
    sizereport({
      gzip: true
    })
  );
});

gulp.task('sprite', function() {
  var spriteData = gulp.src(location + '/assets/images/sprites/*.png').pipe(
    spritesmith({
      retinaSrcFilter: [location + '/assets/images/sprites/*@2x.png'],
      imgName: 'sprite.png',
      retinaImgName: 'sprite@2x.png',
      cssName: '_sprites.scss',
      imgPath: '../assets/images/sprite.png',
      retinaImgPath: '../assets/images/sprite@2x.png'
    })
  );

  // Pipe image stream through image optimizer and onto disk
  spriteData.img.pipe(imagemin()).pipe(gulp.dest(destination + '/assets/images/'));

  spriteData.css.pipe(gulp.dest(destination + '/sass/components/'));
});

gulp.task('generateTestUrls', function() {
  return gulp.src('backstop-base.json')
    .pipe(plumber(plumberErrorHandler))
    .pipe(inject(gulp.src(location + '/_patterns/**/*.twig', {read: false}), {
      starttag: '"scenarios": [',
      endtag: `],`,
      transform: function (filepath, file, i, length) {
        var filepath = filepath.substr(9);
        var parts = filepath.split('/');
        var path;
        if (parts.length === 4) {
          path = parts[0] + '/' + parts[1] + '-' + parts[2] + '/index.html';
        } else {
          path = parts[0] + '/' + parts[1] + '-' + parts[2] + '-' + parts[3] + '-' + parts[3] + '/' + parts[1] + '-' + parts[2] + '-' + parts[3] + '-' + parts[3] + '.html';
        }
        var url = process.platform === 'linux' ? 'http://localhost:8080/' + path : 'http://host.docker.internal:8080/' + path;
        return `{
          "label": "` + parts[parts.length - 1].replace('.twig', '') + `",
          "cookiePath": "backstop_data/engine_scripts/cookies.json",
          "url": "` + url + `",
          "referenceUrl": "",
          "readyEvent": "",
          "readySelector": "",
          "delay": 500,
          "hideSelectors": [],
          "removeSelectors": [],
          "hoverSelector": "",
          "clickSelector": "",
          "postInteractionWait": 0,
          "selectors": [],
          "selectorExpansion": true,
          "expect": 0,
          "misMatchThreshold" : 0.1,
          "requireSameDimensions": true
        }` + (i + 1 < length ? ',' : '');
      }
    }))
    .pipe(rename('backstop.json'))
    .pipe(gulp.dest('.'));
});

gulp.task('jshint', function() {
  return gulp
    .src([location + '/js/src/*.js', location + '/_patterns/**/src/js/*.js'])
    .pipe(plumber(plumberErrorHandler))
    .pipe(jshint({ esnext: true }))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(filter('**/*.js')) // Filtering stream to only js files
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function() {
  return gulp
    .src([location + '/js/src/*.js', location + '/_patterns/**/src/js/*.js'])
    .pipe(plumber(plumberErrorHandler))
    .pipe(
      babel({
        presets: ['es2015'] // ES6
      })
    )
    .pipe(concat('script.min.js'))
    .pipe(gulpif(isProd, stripDebug()))
    .pipe(gulpif(isProd, uglify()))
    .pipe(gulp.dest(destination + '/js/'))
    .pipe(notify({ message: 'Scripts task complete: combined, debug stripped, uglified' }));
});

gulp.task('libraries', function() {
  return gulp
    .src(location + '/js/libraries/*.js')
    .pipe(plumber(plumberErrorHandler))
    .pipe(gulpif(isProd, stripDebug()))
    .pipe(gulpif(isProd, uglify()))
    .pipe(gulp.dest(destination + '/js/'))
    .pipe(notify({ message: 'Scripts task complete: combined, debug stripped, uglified' }));
});

gulp.task('browser-sync', function() {
  browserSync.init(null, {
    /*
         If you comment out the next line BrowserSync will provide the JS code that needs to be
         inserted into the HTML/Template for it to still work on your normal server & URL.
         */
    proxy: 'http://localhost:8080'
  });
});

gulp.task('bs', ['sass', 'browser-sync', 'jshint', 'scripts', 'libraries', 'sizereport'], function() {
  //do stuff after 'sass', 'sprite', 'browser-sync' etc are done.
  gulp.watch(location + '/sass/**/*.{scss,sass}', ['sass']);
  gulp.watch(location + '/js/src/**/*.js', ['jshint']);
  gulp.watch(location + '/js/src/*.js', ['scripts']);
  gulp.watch(location + '/css/*.css', ['sizereport']);
  gulp.watch(location + '/js/libraries/*.js', ['libraries']);
});

gulp.task(
  'default',
  ['sass', 'jshint', 'scripts', 'libraries', 'sizereport', 'generateTestUrls'],
  function() {
    //do stuff after 'sass', 'jshint' etc are done.
    gulp.watch([location + '/sass/**/*.scss', location + '/_patterns/**/sass/*.{sass,scss}'], ['sass']);
    gulp.watch(location + '/_patterns/**/src/js/*.js', ['scripts', 'jshint']);
    gulp.watch(location + '/js/src/**/*.js', ['jshint']);
    gulp.watch(location + '/js/src/*.js', ['scripts']);
    gulp.watch(location + '/css/*.css', ['sizereport']);
    gulp.watch(location + '/js/libraries/*.js', ['libraries']);
  }
);

gulp.task('prod', ['sass', 'sprite', 'jshint', 'scripts', 'libraries', 'sizereport', 'generateTestUrls'], function() {});
