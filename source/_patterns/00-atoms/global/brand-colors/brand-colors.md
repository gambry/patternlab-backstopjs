---
title: Brand Colors
---

This color palette contains brand colours to be used throughout the interface. 
It's supposed to be used as reference, and NOT be used as a pattern.

Also no style is provided as part of this pattern, because this is meant to be just for documentation. Brand Colors
style can be found in <pattern-library>/scss/1-base/_fonts.scss .