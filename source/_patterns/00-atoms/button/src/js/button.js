/* global jQuery */

(function($){
  $('[data-behavior="button"]').on('click', (e) => {
    e.preventDefault();
    $(e.target).toggleClass('button--clicked');
  });
})(jQuery);