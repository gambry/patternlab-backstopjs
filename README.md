# PatternLab (twig) + BackstopJS #

This is a demo project, showcasing the capabilities of PatternLab + BackstopJS as visual regression tests framework.

## Built With

* [PatternLab](https://patternlab.io/) 2.x - Pattern Library description and visualisation framework.
* [Composer](https://getcomposer.org/) - PHP Package Manager
* [Twig](https://twig.symfony.com/) - Template Engine for PHP
* [Gulp](https://gulpjs.com/) - NodeJS tasks automation tool.
* [BackstopJS](https://garris.github.io/BackstopJS/) - Visual Regression testing tool.

### Prerequisites

The following tools are required to install, build and visualise the Pattern Library:
* PHP >= 7.1
* Node >= 8
* npm >= 6.2

### Installing

Setup your local instance by running:
* `composer install` to download PatternLab dependencies.
* `npm ci` to download gulp dependencies.

### Testing

This repository uses BackstopJS for visual regression testing. It tests each pattern on it's own individual url.
These urls are generated in a gulp task `gulp generateTestUrls` and are then injected into the `backstop.json`
config file.

BackstopJS uses [Chrome headless](https://developers.google.com/web/updates/2017/04/headless-chrome) to render the
patterns HTML/CSS/JS as screenshots. Due the differences while running Chrome in different OSs, the recommended way of
running the tests is through a docker image. That's also the way GitlabCI will run the tests and so we are sure testing and
creating the referencing images is done using the same underlying OS and libraries.

If you don't have any of the required tools below (node, php, gulp) `.gitlab-ci.yml` file contains CI/CD instructions, which
are complete docker-ised.

So in order to run the tests you need:

A. If this is the first time you are running the tests:
1. Make sure `public/` static files are up to date, by eventually running `php core/console --generate`.
2. Run the console server with: `php core/console --server --host 127.0.0.1` (**NOT** `localhost`!)
3. Generates the tests scenarios for your modules with `gulp generateTestUrls`.
4. Run tests with `docker run --rm --net="host" -v $(pwd):/src backstopjs/backstopjs test`
5. All tests should be green.

B. If you have worked on a module and now want to integrate it in the tests pipelines:
1. Make sure `public/` static files are up to date, by eventually running `php core/console --generate`.
2. Run the console server with: `php core/console --server --host 127.0.0.1` (**NOT** `localhost`!)
3. Check any existing tests are passing with `docker run --rm --net="host" -v $(pwd):/src backstopjs/backstopjs test`
4. If all tests are passing you're ready to add your new module(s) to the test reference files, execute `gulp generateTestUrls`.
5. Re-run the tests `docker run --rm --net="host" -v $(pwd):/src backstopjs/backstopjs test`
6. All tests should still pass, except **expected failures** from new module(s) or valid changes.
7. Generate the references with `docker run --rm --net="host" -v $(pwd):/src backstopjs/backstopjs reference`. ***Please
 make sure to regenerate the images ONLY IF it's a valid change***.

#### Debug tests failure locally

By default only junit report is produces ([see reference](https://github.com/garris/BackstopJS#reporting-workflow-tips)),
 so use `node node_modules/backstopjs/cli/index.js openReport` to open the visual report too in your favourite browser.

#### Debug tests failure on CI

On each Merge Request a Test summary will give you a brief of what test failed and what passed.

On GitlabCI failure you should always try and run the tests locally, to identify the issue. In the eventuality tests pass
locally, but still fail on Gitlab the `backstop_data` folder is published as job artifact. You can download it - you
see a download button in the failing Job row in the Pipelines > Jobs listing page - and place it locally in your project
root, making sure to delete it if already existing. Now execute `node node_modules/backstopjs/cli/index.js openReport`
to visualise why tests have failed.